package com.llaerto.keepmymoney.usecases.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.llaerto.keepmymoney.R
import com.llaerto.keepmymoney.base.BaseFragment
import com.llaerto.keepmymoney.databinding.FragmentHomeBinding
import com.llaerto.keepmymoney.model.Category
import com.llaerto.keepmymoney.model.Record
import com.llaerto.keepmymoney.model.RecordRow
import com.llaerto.keepmymoney.model.toRecordRow
import com.llaerto.keepmymoney.utils.ItemDecorator
import com.llaerto.keepmymoney.utils.SwipeController
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class HomeFragment : BaseFragment<HomeState, HomeActions, HomeViewModel, FragmentHomeBinding>() {

    override val viewModel: HomeViewModel by viewModel()
    override var _binding: FragmentHomeBinding? = null
    private val adapter: HomeAdapter = HomeAdapter {
        onItemClick(it.id)
    }

    override fun applyViewState(viewState: HomeState) {
        //nothing to do
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun onItemClick(id: Int) {
        //todo handle item click
    }

    private fun initRecyclerView() {
        //todo remove it after implementation real data
        val list = mutableListOf(
            Record(0, true, Date().time, Category(0, "#BC0606", "food"), 22.00, "$", "some comment"),
            Record(1, false, Date().time, Category(1, "#2CB632", "car"), 33.99, "$", null),
            Record(2, false, Date().time, Category(2, "#757575", "house"), 4.49, "$", null),
            Record(4, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(3, true, Date().time, Category(3, "#212121", "medicine"), 123.11, "$", null),
            Record(3, true, Date().time, Category(3451, "#212121", "medicine"), 1208903.11, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(6, false, Date().time, Category(4, "#009688", "fun"), 931.00, "$", null),
            Record(5, true, Date().time, Category(5, "#607D8B", "baby"), 23412.00, "$", null)
        )
        val listOfROws: MutableList<RecordRow> = list.map { it.toRecordRow() }.toMutableList()
        adapter.setItems(listOfROws)
        binding.recordList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recordList.addItemDecoration(ItemDecorator())
        binding.recordList.adapter = adapter
        val swipeHelper = SwipeController { position ->
            val recordToRemove = adapter.getItemByPosition(position)
            adapter.removeItem(position)
            val snackbar = Snackbar
                .make(binding.recordList, getString(R.string.snack_item_removed), Snackbar.LENGTH_LONG)
            snackbar.setAction(getString(R.string.snack_undo)) {
                adapter.restoreItem(recordToRemove, position)
            }
            snackbar.setActionTextColor(requireContext().getColor(R.color.colorAccent))
            snackbar.show()
        }
        swipeHelper.attachToRecyclerView(binding.recordList)
    }
}