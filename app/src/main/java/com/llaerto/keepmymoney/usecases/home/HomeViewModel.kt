package com.llaerto.keepmymoney.usecases.home

import com.llaerto.keepmymoney.base.BaseViewModel

class HomeViewModel() : BaseViewModel<HomeState, HomeActions>() {

    override fun initialState() = HomeState.EmptyState
}
