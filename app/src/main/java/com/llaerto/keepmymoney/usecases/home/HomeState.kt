package com.llaerto.keepmymoney.usecases.home

import com.llaerto.keepmymoney.base.BaseViewAction
import com.llaerto.keepmymoney.base.BaseViewState

sealed class HomeState : BaseViewState {
    object EmptyState : HomeState()
}

sealed class HomeActions : BaseViewAction